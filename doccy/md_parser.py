import os

from os import walk
from pathlib import Path
from doccy.parser import Parser
from regexer import RegexerString as restr

class MD(Parser):
    """Parse markdown files and generate coresponding type of document.
    """
    def __init__(self, writer):
        self.writer = writer()

    def generate(self, root = os.getcwd()):
        """Generate document in the coresponding type.

        Args:
            root (str, optional): Sub path to doc. Defaults to os.getcwd().

        Yields:
            Path: paths to created document files 
        """
        for (path, _, files) in walk(root):
            if restr(path)[r"[\\\/]\."] \
                or str(Path(root) / 'doc') + os.path.sep in path:
                continue
            for f in files:
                if f == 'README.md':
                    continue
                if f.endswith('.md'):
                    rr = restr(path.replace(root, '')) - r'^\\' - r'\\$'
                    self._write(rr, f)
                    yield Path(path.replace(root, '')) / f
    
    def _write(self, path, md_file):
        with open(Path(path) / md_file, 'r') as f:
            path = Path('doc') / self.writer.type / path
            with self.writer.module(path, restr(md_file)-r'\.md$') as write:
                for l in f.readlines():
                    if l.startswith('#'):
                        l = restr(l) - r'\n$' - r'^#+\s'
                        write.heading(l, len(l["^#+"]))