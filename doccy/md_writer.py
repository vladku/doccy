from pathlib import Path

class MDWriter:
    """Write markdown text formating.
    """
    def __init__(self):
        self.type = 'md'

    class Module:
        """Simplify/unify document formating.
        """   
        def __init__(self, file_name):
            self.file = open(f"{file_name}.md", "w")

        def __enter__(self):
            return self

        def __exit__(self, type, value, traceback):
            self.file.close()

        def ref(self, name, parent):
            """Add reference to other document

            Args:
                name (str): reference text
                parent (str): path to referenced file
            """
            self.paragraph(f'* [{name}]({parent}/{name}.md)')

        def heading(self, text, level):
            """Add heading with corresponding head level

            Args:
                text (str): Head text
                level (int): Heading level
            """
            self.file.write(f"{'#'*level} {text}\n\n")

        def paragraph(self, text):
            """Add paragraph

            Args:
                text (str): Text to add
            """
            self.file.write(f'{text}\n\n')

        def _list(self, elements, point):
            elements = list(filter(None, elements))
            if not elements:
                return
            ll = len(elements[0]) - len(elements[0].lstrip())
            for e in elements:
                self.file.write(f'{point}{e[ll:].rstrip()}\n\n')
            self.file.write('\n')

        def point_list(self, elements):
            """Add pointed list

            Args:
                elements (list): Items to add
            """
            self._list(elements, '- ')

        def numbered_list(self, elements):
            """Add numbered list

            Args:
                elements (list): Items to add
            """
            self._list(elements, '# ')

        def code(self, text, language='python'):
            """Add code block

            Args:
                text (str): Code block text
                language (str, optional): Code language. Defaults to 'python'.
            """
            self.file.write(f'```{language}\n{text}\n```\n\n')

    def module(self, path, name):
        """Create md file and return object that helps write document

        Args:
            path (Path): Path to the file
            name (str): Name of file

        Returns:
            [Module]: Object to work with md file to simplify/unify document formating
        """     
        if not path.exists():
            if not path.parent.exists():
                path.parent.mkdir()
            path.mkdir()
        return self.Module(path / name)