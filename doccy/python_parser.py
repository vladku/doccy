import os
import ast, _ast

from os import walk
from pathlib import Path
from doccy.parser import Parser
from regexer import RegexerString as restr

class Python(Parser):
    """Parse python files and generate coresponding type of document.
    """
    def __init__(self, writer):
        self.writer = writer()

    def method(self, write, method):
        """Generate document text for python function.

        Args:
            write (Module): Object to write document
            method (str): Python function doc string 
        """
        for part in method.split("\n\n"):
            rpart = restr(part)
            if part.startswith('Args:'):
                self._args(write, rpart[r'\n\s+(?P<name>\w+)\s?(\((?P<type>\w+)\))?:\s?(?P<desc>.+)'])
            elif part.startswith('Returns:'):
                self._returns(write, rpart[r'Returns:\n?(?P<desc>(.*\s*)*)', 'desc'])
            elif part.startswith('Yields:'):
                self._returns(write, rpart[r'Yields:\n?(?P<desc>(.*\s*)*)', 'desc'], 'Yields')
            elif part.startswith('Raises:'):
                write.heading("Raises:", 5)
                raises = rpart[r'Raises:(?P<desc>(.*\s*)*)', 'desc'].split("\n")
                write.point_list(raises)
            elif part.startswith('>>>'):
                write.heading("Example:", 5)
                write.code(part)
            else:
                write.paragraph(part)
    
    def _args(self, write, args):
        """Write function args

        Args:            
            write (Module): Object to write document
            args (RegexerString): Function arguments
        """
        write.heading("Args:", 5)
        write.point_list([f"{arg['name']} `{arg['type']}`: {arg['desc']}"
            for arg in args])

    def _returns(self, write, lines, head="Returns"):
        write.heading(f"{head}:", 5)
        for l in lines.split("\n"):
            write.paragraph(l.strip())

    def generate(self, root = os.getcwd()): 
        """Generate document in the coresponding type.

        Args:
            root (str, optional): Sub path to doc. Defaults to os.getcwd().

        Yields:
            Path: paths to created document files 
        """       
        for (path, _, files) in walk(root):
            if restr(path)[r"[\\\/]\."]:
                continue
            for f in files:
                if f.endswith('.py') and f != 'setup.py':
                    rr = restr(path.replace(os.getcwd(), '')) - r'^\\' - r'\\$'
                    self._write(rr, f)
                    yield Path(path.replace(root, '')) / f
    
    def _write(self, path, md_file):
        with open(Path(path) / md_file, 'r') as f:
            npath = Path(path)
            if md_file == '__init__.py':
                md_file = Path(path).parts[-1]
                npath = npath.parent
                elems = [ast.parse(f.read())]
            else:
                elems = ast.parse(f.read()).body
            npath = Path('doc') / self.writer.type / npath
            with self.writer.module(npath, restr(md_file)-r'\.py$') as w:
                for elem in elems:
                    if type(elem) == _ast.Module:
                        moduls = [x for x in os.listdir(path) if x.endswith('.py') and not x.startswith('__')]
                        w.heading(md_file, 1)
                        w.paragraph(ast.get_docstring(elem))
                        w.heading("Modules", 2)
                        for m in moduls:
                            w.ref(restr(m)-r'\.py$', path)
                    if type(elem) == _ast.ClassDef:
                        w.heading(md_file, 2)
                        w.paragraph(ast.get_docstring(elem))
                        w.heading("Methods", 3)
                        for m in [x for x in elem.body if type(x) == _ast.FunctionDef]:
                            w.heading(m.name, 4)
                            self.method(w, str(ast.get_docstring(m)))
                            