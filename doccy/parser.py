from abc import ABC, abstractmethod

class Parser(ABC):
    @abstractmethod
    def generate(self):
        pass