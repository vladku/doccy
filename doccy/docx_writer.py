import os
from docx import Document

class DocxWriter:
    '''
    Write docx text formating
    '''
    def __init__(self):
        self.type = 'docx'
        self.level = 0
        self.file = Document()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.file.save(os.path.join('doc', 'Doc.docx'))

    def ref(self, name, parent):
        pass

    def heading(self, text, level):
        """Add heading with corresponding head level

        Args:
            text (str): Head text
            level (int): Heading level
        """
        self.file.add_heading(text, level + self.level)

    def paragraph(self, text):
        """Add paragraph to document.

        Args:
            text (str): text to add
        """
        self.file.add_paragraph(text)

    def _list(self, elements, style):
        for e in filter(None, elements):
            self.file.add_paragraph(e, style=style)

    def point_list(self, elements):
        """Add pointed list

        Args:
            elements (list): Items to add
        """
        self._list(elements, style='List Bullet')

    def numbered_list(self, elements):
        """Add numbered list

        Args:
            elements (list): Items to add
        """
        self._list(elements, style='List Number')

    def code(self, text, language='python'):
        """Add code block

        Args:
            text (str): Code block text
            language (str, optional): Code language. Defaults to 'python'.
        """
        p = self.file.add_paragraph(text)
        p.style.font.name = 'Consolas'
    
    def module(self, path, name):
        """Create md file and return object that helps write document

        Args:
            path (Path): Path to the file
            name (str): Name of file

        Returns:
            [Module]: Object to work with md file to simplify/unify document formatting
        """     
        self.level = path.count('.')
        self.file.add_heading(name, self.level)
        return self
