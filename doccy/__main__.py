"""Generate project documentation in the different formats.

Usage:
    doccy [--writer=Writer]
    
"""
import os
import docopt
import pkg_resources

from pathlib import Path

class Generator:
    def __init__(self, generator):
        self.generator = generator
    def generate(self):
        path = Path('doc')
        if not path.exists():
            path.mkdir()
        modules = self.generator.generate()
        readme = ''
        path /= 'root.md'
        if not path.exists():
            with open("README.md", "r") as file:
                readme = file.read()        
            readme += f"\n## Modules:\n"
        else:
            with open(path, "r") as file:
                readme = file.read() 
        for m in modules:
            readme += f"\n1. [{m}]({m}{'' if str(m).endswith('.md') else '.md'})"
        with open(path, "w") as file:
            file.write(readme)

def main():
    args = docopt.docopt(__doc__)
    writer = args['--writer'] or 'md'
    doc_writers = {}
    doc_types = {}
    path = Path('doc') / 'root.md'
    if path.exists():
        os.remove(path)
    for entry_point in pkg_resources.iter_entry_points('doc_writers'):
        doc_writers[entry_point.name] = entry_point.load()
    for entry_point in pkg_resources.iter_entry_points('doc_types'):
        doc_types[entry_point.name] = entry_point.load()    
    for doc in doc_types.values():
        Generator(doc(doc_writers[writer])).generate()
    
if __name__ == '__main__':
    main()