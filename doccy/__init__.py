'''
Library to simplify generation of documentation for your project.
It is combine documentation for different types of files like .py or .md, 
also supports different type of result doc like MarkDown or Word document.
Types of supported source and result can be simply extended by Python plugin,
add doc_writers or doc_types entry point for your plugin.
'''