import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt', 'r') as r:
    requirements = r.readlines()
    
setuptools.setup(
    name='doccy',
    version='2020.10.04',
    author="Kurovkyi Vladyslav",
    packages=setuptools.find_packages(),
    description='doccy',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/vladku/doccy",
    python_requires=">=3.6",
    py_modules=["doccy"],
    license="MIT",
    entry_points = {
        'console_scripts': ['doccy=doccy.__main__:main'],
        'doc_writers': [
            'md = doccy.md_writer:MDWriter',
            'rst = doccy.rst_writer:RSTWriter',
            'docx = doccy.docx_writer:DocxWriter',
        ],
        'doc_types': [
            'md = doccy.md_parser:MD',
            'docx = doccy.python_parser:Python',
        ],
    },
    install_requires=requirements,
    extras_require={
        'dev': [
            'pytest',
            'pytest-cov'
        ]
    }
)